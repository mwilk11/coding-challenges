from confluent_kafka import Consumer
import json


def receive_data(topic: str) -> list:
    conf = {'bootstrap.servers': 'localhost:9092', 
            'group.id':'cons_grp',
            'auto.offset.reset':'earliest'}
    consumer = Consumer(conf)
    consumer.subscribe([topic])
    data = []
    while True:
        message = consumer.poll(timeout=2.0)
        if message is None:
            break 
        if message.error():
            print(f'Error: {message.error()}')
        msg = message.value().decode('UTF-8')
        data.append(json.loads(msg))
    consumer.close()
    return data


def process_data(data: list) -> list:
    proc_data = []
    for msg in data:
        row = {}
        full_name = f"{msg['first_name']}_{msg['last_name']}"
        key1, key2 = 'date_of_birth', 'registered_voter'
        row[full_name] = {key1: msg[key1], key2: msg[key2]}
        proc_data.append(row)
    return proc_data


def write_data(data: list, file_name: str) -> None:
    with open(file_name, 'w') as f:
        for msg in data:
            json.dump(msg, f)
            f.write('\n')


if __name__ == "__main__":
    received_data = receive_data("test-topic")
    processed_data = process_data(received_data)
    write_data(processed_data, "processed_data.jsonl")
