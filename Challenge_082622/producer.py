from confluent_kafka import Producer
from datetime import datetime
import json
import time


def read_data(path: str, header: bool = True) -> dict:
    with open(path, 'r') as f:
        lines_list = f.readlines()
    data = {}
    for line in lines_list:
        line = line.strip().split(',')
        if header:
            headers_list = line
            header = False
        else:
            data[line[0]] = {headers_list[i]: line[i] for i in range(1, len(line))}
    return data


def process_data(data: dict) -> dict:
    for idx in data.keys():
        data[idx]['first_name'] = data[idx]['first_name'].strip(' 1234567890 ').upper()
        data[idx]['last_name'] = data[idx]['last_name'].strip(' 1234567890 ').upper()
        data[idx]['date_of_birth'] = datetime.strptime(data[idx]['date_of_birth'], '%m/%d/%Y').strftime('%Y-%m-%d')
        if data[idx]['registered_voter'] == 'yes':
            data[idx]['registered_voter'] = True
        else:
            data[idx]['registered_voter'] = False
    return data


def send_data(data: dict, kafka_topic: str) -> None:
    conf = {'bootstrap.servers': 'localhost:9092'}
    producer = Producer(conf)
    for idx in data.keys():
        message = json.dumps(data[idx])
        producer.produce(kafka_topic, key=idx, value=message)
        producer.flush()
        time.sleep(1)


if __name__ == "__main__":
    raw_data = read_data("/Users/mikolaj/Desktop/helm/file_one.csv")
    processed_data = process_data(raw_data)
    send_data(processed_data, "test-topic")
