#AWS Lambda function

import boto3
from botocore.client import Config
from botocore import UNSIGNED
import json

def lambda_handler(event, context):
    data = json.loads(event['body'])
    instrument = data['instrument']
    
    #check if passed instrument is valid
    if instrument != 'EM302' and instrument != 'EM710':
        message = 'instrument is not valid'
        response = {'statusCode': 200, 'body': message}
        return response
        
    # set variables 
    bucket_name = 'noaa-wcsd-pds'
    bucket_prefix = f'data/raw/Falkor/FK007/{instrument}/'
    file_count = 0
    
    # set s3 configuration and bucket 
    s3 = boto3.resource('s3',
		  aws_access_key_id='',
		  aws_secret_access_key='',
		  config=Config(signature_version=UNSIGNED))

    bucket = s3.Bucket(name=bucket_name)
    
    # count total objects with wcd file extensions
    for file in bucket.objects.filter(Prefix=bucket_prefix):
        file_name = file.key
        if file_name.endswith('wcd'):
            file_count += 1
            
    response = {'statusCode': 200,
    		'headers': {},
        	'body': file_count
    			}
    return response
