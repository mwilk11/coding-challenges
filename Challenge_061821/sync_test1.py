import boto3
from botocore.client import Config
from botocore import UNSIGNED
import os

# set variables 
bucket_name = 'noaa-wcsd-pds'
bucket_prefix = 'data/raw/Falkor/FK007/EM302/'
file_count = 0

# set s3 configuration and bucket 
s3 = boto3.resource('s3',
	aws_access_key_id='',
	aws_secret_access_key='',
	config=Config(signature_version=UNSIGNED))

bucket = s3.Bucket(name=bucket_name)

# loop through each object within bucket
# return total count of objects with wcd file extension
for file in bucket.objects.filter(Prefix=bucket_prefix):
	file_ext = os.path.splitext(file.key)[1]
	if file_ext == '.wcd':
		file_count += 1

print(file_count)
