# input: 1234 (int)
# output: "1,234" (str)

def comma_func(some_int: int):
    init_str = str(some_int)
    final_str = init_str[0]
    end = len(init_str)
    count = 1
    for letter in init_str[1:]:
        if (end - count) % 3 == 0:
            final_str += ','
        final_str += letter
        count += 1
        
    return final_str

print(comma_func(-1234))


#Non-iterative solution to reverse a string
#reverse(‘abcd’) -> ‘dcba’

def reverse(some_string: str):
    #base case when string is empty
    if some_string == '':
        return ''
    else:
        new_string = some_string[0:len(some_string)-1]
        return some_string[-1] + reverse(new_string)

print(reverse('abcd'))

'''
reverse('abcd')
=> d + reverse('abc')
=> d + c + reverse('ab')
=> d + c + b + reverse('a')
=> d + c + b + a + ''
=> d + c + b + a 
=> d + c + ba
=> d + cba
=> dcba
'''
