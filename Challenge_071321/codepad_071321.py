# Write a function that takes one argument: a single string comprised of zero or more '1's, '0's and/or '?'s.
# The function should return a list of all strings you can make by replacing each '?' in the input string with either
# '1' or '0'.

# Example: If the input is '01??' the output should be ['0100','0101','0110','0111'].

# Notes:
#   (a) You can assume the input string is well-formed, 
#       i.e. it will always be a string and always comprised of 0+ '1's, '0's, and/or '?'s.
#   (b) The returned list does not need to be in any particular order.

def quest_replace(some_string):

    if some_string[0] == '?':
        str_list = ['0', '1']
    else:
        str_list = [some_string[0]]

    for char in some_string[1:]:
        if char == '?':
            for i in range(len(str_list)):
                temp = str_list[i]
                str_list[i] = str_list[i] + '0'
                str_list.append(temp + '1')
        else:
            for i in range(len(str_list)):
                str_list[i] += char

    return str_list


ans_list = quest_replace('01??')
print(ans_list)
        