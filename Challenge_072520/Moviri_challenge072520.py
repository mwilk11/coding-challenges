def log_net_band_util(filename1:str, filename2:str):

	"""Create a log output with Timestamp, Server, Network Interface and Network Bandwidth Utilization using two input files."""


	with open(filename1) as bandwidth_data:
		file1_listoflines = bandwidth_data.read().splitlines()

	with open(filename2) as netbitrate_logs:
		file2_listoflines = netbitrate_logs.read().splitlines()


	#After reading both files and splitting them up into a list of lines without newline characters, initialize a bandwidth look-up dictionary

	bandwidth_dict = {}


	#Loop through each line (omitting the header) in the bandwidth data file, splitting on each comma to populate the bandwidth dictionary
	#Dictionary keys are tuples of Server and Interface Name (both strings) with the value for each key being the bandwidth (string), example: ('server1', 'eth0'): '20'

	for line in file1_listoflines[1:]:
		data_item = line.split(',')
		bandwidth_dict[tuple(data_item[0:2])] = data_item[2]


	#print a header to understand the format of each line of the log

	print("Timestamp | Server | Network Interface | Network Bandwidth Utilization")


	#Loop through each line of the netbitrate log file (omitting the header), splitting on each comma, and calculate the network bandwidth utilization 
	#Network Bandwidth Utilization = Network Bit Rate / Bandwidth 
	#Retrieve the Bandwidth values from the Bandwidth dictionary by passing in a tuple of the Server and Interface Name then converting the value to float
	#print each new log line in the format of the header above 

	for line in file2_listoflines[1:]:
		log_item = line.split(',')
		net_band_util = float(log_item[3]) / float(bandwidth_dict[tuple(log_item[1:3])])
		print(log_item[0], log_item[1], log_item[2], net_band_util)


log_net_band_util("bandwidth.csv", "netbitrate.csv")