#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


data = pd.read_csv("<filename>.csv")


# In[3]:


data.loc[:,'date'] =  pd.to_datetime(data.loc[:,'date'])


# In[4]:


mask = (data['date'] >= '2020-03-01') & (data['date'] <= '2020-03-31')


# In[5]:


data[['widget','engagements','engagement_type']][mask].where(data['engagement_type'][mask]=='page_loads').groupby(['widget','engagement_type']).agg('sum').sort_values('engagements', ascending=False)


# In[6]:


data[['widget','engagements','engagement_type']][mask].where(data['engagement_type'][mask]=='page_loads').groupby(['widget','engagement_type']).agg('sum').sort_values('engagements', ascending=True).plot(kind='barh', legend=False, title='Page Loads by widget in March 2020')
plt.xlabel('Page Loads in log scale')


# In[7]:


page_loads_perday = data[['date','widget','engagements','engagement_type']].where((data['widget']=='depositrates') & (data['engagement_type']=='page_loads')).groupby('date').sum()


# In[8]:


total_eng_perday = data[['date','widget','engagements','engagement_type']].where((data['widget']=='depositrates') & (data['engagement_type']=='total_eng')).groupby('date').sum()


# In[9]:


engagement_rate = total_eng_perday/page_loads_perday


# In[10]:


avg_eng_rate = float(engagement_rate.sum()/engagement_rate.count())


# In[11]:


engagement_rate.plot(legend=False, title='Daily Engagement Rate of depositrates widget')
plt.axhline(avg_eng_rate, color='red')
plt.ylabel('Engagement Rate')


# In[12]:


mask2 = (data['date'] >= '2020-04-01') & (data['date'] <= '2020-04-30')


# In[13]:


total_eng_april = data[['date','widget','engagements','engagement_type']][mask2].where((data['engagement_type'][mask2]=='total_eng')).groupby('widget').sum()


# In[14]:


unique_eng_april = data[['date','widget','engagements','engagement_type']][mask2].where((data['engagement_type'][mask2]=='unique_eng')).groupby('widget').sum()


# In[15]:


eng_peruser_april = total_eng_april/unique_eng_april


# In[16]:


eng_peruser_april.sort_values('engagements',ascending=False)


# In[17]:


eng_peruser_april.sort_values('engagements').plot(kind='barh', legend=False,                                                  title='Engagements/User by widget in April 2020')
plt.xlabel('Engagements per User')


# In[18]:


page_loads_perday = data[['date','widget','engagements','engagement_type']].where((data['widget']=='depositrates') & (data['engagement_type']=='page_loads')).groupby('date').sum()

total_eng_perday = data[['date','widget','engagements','engagement_type']].where((data['widget']=='depositrates') & (data['engagement_type']=='total_eng')).groupby('date').sum()

engagement_rate = total_eng_perday/page_loads_perday

engagement_rate.rolling(window=3).mean().plot(legend=False, title='Daily Engagement Rate of depositrates widget')
plt.ylabel('Engagement Rate')


# In[19]:


page_loads_perday2 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='brokerageaccounts') & (data['engagement_type']=='page_loads')).groupby('date').sum()

total_eng_perday2 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='brokerageaccounts') & (data['engagement_type']=='total_eng')).groupby('date').sum()

engagement_rate2 = total_eng_perday2/page_loads_perday2

engagement_rate2.rolling(window=3).mean().plot(legend=False, title='Daily Engagement Rate of brokerageaccounts widget')
plt.ylabel('Engagement Rate')


# In[20]:


page_loads_perday3 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='mortgagerates') & (data['engagement_type']=='page_loads')).groupby('date').sum()

total_eng_perday3 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='mortgagerates') & (data['engagement_type']=='total_eng')).groupby('date').sum()

engagement_rate3 = total_eng_perday3/page_loads_perday3

engagement_rate3.rolling(window=3).mean().plot(legend=False, title='Daily Engagement Rate of mortgagerates widget')
plt.ylabel('Engagement Rate')


# In[21]:


page_loads_perday4 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='textcontentblock') & (data['engagement_type']=='page_loads')).groupby('date').sum()

total_eng_perday4 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='textcontentblock') & (data['engagement_type']=='total_eng')).groupby('date').sum()

engagement_rate4 = total_eng_perday4/page_loads_perday4

engagement_rate4.rolling(window=3).mean().plot(legend=False, title='Daily Engagement Rate of textcontentblock widget')
plt.ylabel('Engagement Rate')


# In[22]:


page_loads_perday5 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='incometaxcalculator') & (data['engagement_type']=='page_loads')).groupby('date').sum()

total_eng_perday5 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='incometaxcalculator') & (data['engagement_type']=='total_eng')).groupby('date').sum()

engagement_rate5 = total_eng_perday5/page_loads_perday5

engagement_rate5.rolling(window=3).mean().plot(legend=False, title='Daily Engagement Rate of incometaxcalculator widget')
plt.ylabel('Engagement Rate')


# In[23]:


page_loads_perday6 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='fidelitycustomretirement') & (data['engagement_type']=='page_loads')).groupby('date').sum()

total_eng_perday6 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='fidelitycustomretirement') & (data['engagement_type']=='total_eng')).groupby('date').sum()

engagement_rate6 = total_eng_perday6/page_loads_perday6

engagement_rate6.rolling(window=3).mean().plot(legend=False, title='Daily Engagement Rate of fidelitycustomretirement widget')
plt.ylabel('Engagement Rate')


# In[24]:


page_loads_perday7 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='fidelitysocialsecurity') & (data['engagement_type']=='page_loads')).groupby('date').sum()

total_eng_perday7 = data[['date','widget','engagements','engagement_type']].where((data['widget']=='fidelitysocialsecurity') & (data['engagement_type']=='total_eng')).groupby('date').sum()

engagement_rate7 = total_eng_perday7/page_loads_perday7

engagement_rate7.rolling(window=3).mean().plot(legend=False, title='Daily Engagement Rate of fidelitysocialsecurity widget')
plt.ylabel('Engagement Rate')


# In[25]:


data2 = pd.concat([engagement_rate, engagement_rate2, engagement_rate3, engagement_rate4, engagement_rate5,           engagement_rate6, engagement_rate7], axis=1)


# In[26]:


data2.columns = ['depositrates','brokerageaccounts','mortgagerates','textcontentblock','incometaxcalculator',                 'fidelitycustomretirement', 'fidelitysocialsecurity']           


# In[27]:


data2.corr()
